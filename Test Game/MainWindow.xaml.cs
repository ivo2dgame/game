﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using shapes;
using Physicsengine;
using System.Threading;
using System.Runtime.InteropServices;
using MySql.Data.MySqlClient;
using System.Data;
using Objects;
using Enums;
using System.Windows.Media.Animation;
using System.Text.RegularExpressions;
using System.Media;

namespace Test_Game
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Timer timer;
        public Timer timerSpike;
        private const int REFRESHINTERVAL = 5;
        private const int SPAWNINTERVAL = 5000;
        double worldwidth = 800;
        double worldheight = 700;
        bool DrawFirst;
        bool exit;
        bool gameoversound;
        int lastDraw;
        int Score;
        int Scherm;

        SoundPlayer spikeSound = new SoundPlayer(@"..\..\..\ClassLibrary2\Resources\spike.wav");
        SoundPlayer gameoverSound = new SoundPlayer(@"..\..\..\ClassLibrary2\Resources\gameover.wav");

        Random random1 = new Random();
        Player player1 = new Player();
        Coins coin1 = new Coins();

        // keylog
        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(System.Windows.Forms.Keys vkey);
        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(Int32 vKey);
        Thread thread;

        List<Rock> rocks = new List<Rock>();
        List<AliveObject> enemies = new List<AliveObject>();
        List<Spikes> spikesTwo = new List<Spikes>();

        public MainWindow()
        {
            InitializeComponent();
            dgrHighscore.ColumnWidth = 250;
            dgrHighscore.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;

            Scherm = 1;

            Rock Rock1 = new Rock(600, 540, 200, 40);
            Rock Rock2 = new Rock(200, 540, 200, 40);
            Rock Rock3 = new Rock(700, 330, 200, 40);
            Rock Rock4 = new Rock(100, 330, 200, 40);
            Rock Rock5 = new Rock(400, 175, 200, 40);
            
            rocks.Add(Rock1);
            rocks.Add(Rock2);
            rocks.Add(Rock3);
            rocks.Add(Rock4);
            rocks.Add(Rock5);

        }

        void startGame()
        {

            if (thread != null)
            {
                if (thread.IsAlive)
                {
                    thread.Abort();
                    timer.Change(Timeout.Infinite, Timeout.Infinite);
                }
            }

            int xpos = 20;
            int ypos = 680;
            do
            {
                spikesTwo.Add(new Spikes(xpos, ypos));
                xpos += 40;
            }
            while (xpos <= 800);

            timer = new Timer(new TimerCallback(BerekenNieuwePositie), null, 0, REFRESHINTERVAL);
            timerSpike = new Timer(new TimerCallback(drawSpikes), null, 0, SPAWNINTERVAL);

            exit = false;
            Scherm = 2;
            player1 = new Player();

            Score = 0;
            DrawFirst = true;
            lastDraw = 0;

            CompositionTarget.Rendering += CompositionTarget_Rendering;
            thread = new Thread(StartLogging);
            thread.Start();
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            if (exit == true)
            {
                timerSpike.Dispose();
                if (gameoversound == true)
                {
                    gameoverSound.Play();
                    gameoversound = false;
                }
                switchScreen(Scherm);

                ImageBrush ib = new ImageBrush();
                ib.ImageSource = new BitmapImage(new Uri(@"..\..\Game_bloed.png", UriKind.Relative));
                World.Background = ib;
            }
            else
            {
                World.Background = null;
                gameoversound = true;
                World.Children.Clear();
                player1.drawplayer(World);
                foreach (var rocks in rocks)
                {
                    rocks.draw(World);
                }
                try
                {
                    foreach (var spike in spikesTwo)
                    {
                        spike.draw(World);
                    }
                }
                catch
                {

                }
                switchScreen(Scherm);
                DrawCoin();
            }
        }

        private void switchScreen(int screenNo)
        {
            switch (screenNo)
            {
                case 1:
                    //startscherm
                    startScreen(true);
                    gameScreen(false);
                    highscoreInputScreen(false);
                    highscoreScreen(false);
                    break;

                case 2:
                    // gamescherm
                    startScreen(false);
                    gameScreen(true);
                    highscoreInputScreen(false);
                    highscoreScreen(false);
                    break;

                case 3:
                    //highscore invoeren
                    startScreen(false);
                    gameScreen(false);
                    highscoreInputScreen(true);
                    highscoreScreen(false);
                    break;

                case 4:
                    // highscorescherm
                    startScreen(false);
                    gameScreen(false);
                    highscoreInputScreen(false);
                    highscoreScreen(true);
                    break;

                case 5:
                    //game over scherm
                    startScreen(false);
                    gameScreen(false);
                    highscoreInputScreen(false);
                    highscoreScreen(false);
                    break;
            }


            }

        private void startScreen(bool visible)
        {
            if(visible)
            {
                btnStart.Visibility = Visibility.Visible;
                btnHighscore.Visibility = Visibility.Visible;
                iBerg.Visibility = Visibility.Visible;
                iBerg.Visibility = Visibility.Visible;
                iPlayer.Visibility = Visibility.Visible;
                iRock1.Visibility = Visibility.Visible;
                iRock2.Visibility = Visibility.Visible;
                iRock3.Visibility = Visibility.Visible;
                iRock4.Visibility = Visibility.Visible;
            }
            else
            {
                btnStart.Visibility = Visibility.Collapsed;
                btnHighscore.Visibility = Visibility.Collapsed;
                iBerg.Visibility = Visibility.Collapsed;
                iBerg.Visibility = Visibility.Collapsed;
                iPlayer.Visibility = Visibility.Collapsed;
                iRock1.Visibility = Visibility.Collapsed;
                iRock2.Visibility = Visibility.Collapsed;
                iRock3.Visibility = Visibility.Collapsed;
                iRock4.Visibility = Visibility.Collapsed;

            }
        }

        private void gameScreen(bool visible)
        {
            if (visible)
            {
                txtScore.Visibility = Visibility.Visible;
                lblScore.Visibility = Visibility.Visible;
                border1.Visibility = Visibility.Visible;
                World.Visibility = Visibility.Visible;
            }
            else
            {
                txtScore.Visibility = Visibility.Collapsed;
                lblScore.Visibility = Visibility.Collapsed;
            }
        }

        private void highscoreInputScreen(bool visible)
        {
            if (visible)
            {
                lblHighscoreTitel.Visibility = Visibility.Visible;
                lblHighScore.Visibility = Visibility.Visible;
                lblJouwScore.Visibility = Visibility.Visible;
                txtNaam.Visibility = Visibility.Visible;
                lblfout.Visibility = Visibility.Visible;
                lblHighScore.Content = Score.ToString();
                btnOk.Visibility = Visibility.Visible;
                lblGameOver.Visibility = Visibility.Visible;
            }
            else
            {
                lblHighscoreTitel.Visibility = Visibility.Collapsed;
                lblHighScore.Visibility = Visibility.Collapsed;
                lblJouwScore.Visibility = Visibility.Collapsed;
                txtNaam.Visibility = Visibility.Collapsed;
                lblfout.Visibility = Visibility.Collapsed;
                txtNaam.Text = "";
                lblfout.Content = "";
                btnOk.Visibility = Visibility.Collapsed;
                lblGameOver.Visibility = Visibility.Collapsed;
            }
        }

        private void highscoreScreen(bool visible)
        {
            if (visible)
            {
                dgrHighscore.Visibility = Visibility.Visible;
                btnTerug.Visibility = Visibility.Visible;
                lblHighscorelist.Visibility = Visibility.Visible;
            }
            else
            {
                dgrHighscore.Visibility = Visibility.Collapsed;
                btnTerug.Visibility = Visibility.Collapsed;
                lblHighscorelist.Visibility = Visibility.Collapsed;
            }
        }

        void drawSpikes(object o)
        {
            spikesTwo.Add(new Spikes());

            spikeSound.Play();

        }

        private void DrawCoin()
        {
            if (DrawFirst || Collisiondetection.CollisionDetection(player1.OutsideRectangle, coin1.OutsideRectangle) != 0)
            {
                int Position;
                do
                {
                    Position = random1.Next(1, 7);
                } while (Position == lastDraw);

                lastDraw = Position;

                coin1.newPosition(Position, DrawFirst);

                if (DrawFirst == false)
                {
                    Score += 1;
                }

                txtScore.Text = Score.ToString();
                DrawFirst = false;
            }

            coin1.draw(World);

        }

        private void BerekenNieuwePositie(object o)
        {

            foreach (var rocks in rocks)
            {
                int direction = Collisiondetection.CollisionDetection(player1.OutsideRectangle, worldwidth, worldheight);
                player1.SpeedChangeCollision(direction, 0);

                direction = Collisiondetection.CollisionDetection(player1.OutsideRectangle, rocks.OutsideRectangle);
                double indentation = Collisiondetection.CollisionIndentation(direction, player1.OutsideRectangle, rocks.OutsideRectangle);
                player1.SpeedChangeCollision(direction, indentation);
            }
        try {
            foreach (var spike in spikesTwo)
            {
                int direction = Collisiondetection.CollisionDetection(player1.InsideRectangle, spike.OutsideRectangle);
                if (direction != 0)
                {
                    spike.Dispose();
                    exit = true;
                    Scherm = 3;
                }
                   // stop game                
            }
            }
            catch {
            }

            player1.BerekenNieuwePositie();


        }
        void StartLogging()
        {

            while (true)
            {
                //sleeping for while, this will reduce load on cpu
                Thread.Sleep(10);
                int richting = 0;
                if ((GetAsyncKeyState(System.Windows.Forms.Keys.Left) <= -32767) && exit == false)
                {
                    richting = richting + (int)CollisionDirection.Links;
                }
                if ((GetAsyncKeyState(System.Windows.Forms.Keys.Right) <= -32767) && exit == false)
                {
                    richting = richting + (int)CollisionDirection.Rechts;
                }
                if ((GetAsyncKeyState(System.Windows.Forms.Keys.Up) <= -32767) && exit == false)
                {
                    richting = richting + (int)CollisionDirection.boven;
                }
                if ((GetAsyncKeyState(System.Windows.Forms.Keys.Down) <= -32767) && exit == false)
                {
                    richting = richting + (int)CollisionDirection.Onder;
                }

                if ((richting & (int)CollisionDirection.Links) == (int)(CollisionDirection.Links) & (richting & (int)CollisionDirection.Rechts) == (int)(CollisionDirection.Rechts) && exit == false)
                {

                }
                else
                {
                    if ((richting & (int)CollisionDirection.Links) == (int)(CollisionDirection.Links) && exit == false)
                    {
                        player1.SpeedChangeLeft();
                        //     Debug.WriteLine("left");
                    }
                    if ((richting & (int)CollisionDirection.Rechts) == (int)(CollisionDirection.Rechts) && exit == false)
                    {
                        player1.SpeedChangeRight();
                        //    Debug.WriteLine("Rechts");
                    }
                }
                if ((richting & (int)CollisionDirection.boven) == (int)(CollisionDirection.boven) && exit == false)
                {
                    player1.SpeedChangeJump();
                    
                    // Debug.WriteLine("Up");
                }
                if ((richting & (int)CollisionDirection.Onder) == (int)(CollisionDirection.Onder) && exit == false)
                {
                    player1.SpeedChangedown();
                    // Debug.WriteLine("Up");
                }
            }
        }

        void GameOver()
        {
            Scherm = 1;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

        private void OnlineHighscore()
        {
            switchScreen(4);
            Scherm = 4;
                
            DataTable datatable = new DataTable();
            getData("Select naam, score from highscore order by score desc;", out datatable);

            if (datatable != null)
            {dgrHighscore.ItemsSource = datatable.DefaultView;
            }
            exit = true;
            
        }

        public void getData(string sqlcommand, out DataTable datatable)
        {
            try {
                DataTable dataTable = new DataTable();
                //Het wachtwoord voor de MYSQL database staat hier letterlijk in de connection string. 
                //Dit is natuurlijk absoluut niet veilig. Later bekijken hoe je dit best beveiligd.
                string server = "nielsdq89.eightynine.axc.nl";
                string port = "3306";
                string user = "nielsdq89_hs";
                string password = "hs123";
                string database = "nielsdq89_hs";

                string myConnectionString = "SERVER=" + server + ";PORT=" + port + ";user=" + user + ";password=" + password + ";database=" + database;
                MySqlConnection conn = new MySqlConnection(myConnectionString);
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(sqlcommand, conn);
                datatable = new DataTable();
                MySqlDataReader rdr;


                rdr = cmd.ExecuteReader();
                datatable.Load(rdr);
                conn.Close();
            }
            catch
            {
                datatable = null;
                MessageBox.Show("We kunnen momenteel niet verbinden met onze server. Controleer je internetverbinding.");
            }

        }

        public void writeData(string sqlcommand)
        {
            try {
            //Het wachtwoord voor de MYSQL database staat hier letterlijk in de connection string. 
            //Dit is natuurlijk absoluut niet veilig. Later bekijken hoe je dit best beveiligd.
            string server = "nielsdq89.eightynine.axc.nl";
            string port = "3306";
            string user = "nielsdq89_hs";
            string password = "hs123";
            string database = "nielsdq89_hs";

            string myConnectionString = "SERVER=" + server + ";PORT=" + port + ";user=" + user + ";password=" + password + ";database=" + database;
            MySqlConnection conn = new MySqlConnection(myConnectionString);

            MySqlCommand cmd = new MySqlCommand(sqlcommand, conn);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            }
            catch
            {
                MessageBox.Show("We kunnen momenteel niet verbinden met onze server. Controleer je internetverbinding.");
            }

        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            startGame();
        }

        private void bttnGameOver_Click(object sender, RoutedEventArgs e)
        {
            GameOver();
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            string naam = txtNaam.Text;



            if (naam != "" && Regex.IsMatch(naam, @"^[a-zA-Z]+$") && naam.Length < 8)
            {
                writeData("INSERT INTO highscore (Naam, Score) VALUES ('" + naam + "', " + Score + ")");
                World.Children.Clear();
                spikesTwo.Clear();
                border1.Visibility = Visibility.Collapsed;
                GameOver();
                
            }
            else
            {
                lblfout.Content = "De naam die je invoert mag enkel letters bevatten en mag maximum 7 karakters lang zijn!";
            }

        }

        private void btnHighscore_Click(object sender, RoutedEventArgs e)
        {
            OnlineHighscore();
        }

        private void btnTerug_Click(object sender, RoutedEventArgs e)
        {
            switchScreen(1);
            Scherm = 1;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}






