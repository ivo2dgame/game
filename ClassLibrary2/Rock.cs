﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shapes;
using System.Windows.Controls;
using System.Windows.Media;
using System.Drawing;
using System.Windows.Media.Imaging;
using Enums;


namespace Objects
{
    public class Rock : Non_AliveObject
    {
        public Rock()
        {
            Position_X = 600;
            Position_Y = 600;
            Width = 50;
            Height = 50;

            OutsideRectangle = new rectangle(Position_X, Position_Y, Width, Height);

        }
        public Rock(double Position_X, double Position_Y, double Width,double Height)
        {
            this.Position_X = Position_X;
            this.Position_Y = Position_Y;
            this.Width = Width;
            this.Height= Height;
            OutsideRectangle = new rectangle(Position_X, Position_Y, Width, Height);
        }

        public void draw(Canvas World)
        {

            // ik zal de rechthoeken tekenen om de bewegingen te kunnen testen
            // drawing outside rectangle 
            System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
            rect.Height = OutsideRectangle.Height;
            rect.Width = OutsideRectangle.Width;

            rect.Fill = new SolidColorBrush(Colors.Red);
            Canvas.SetLeft(rect, OutsideRectangle.Position_X - OutsideRectangle.Width / 2);
            Canvas.SetTop(rect, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2);
            World.Children.Add(rect);

            //image of rock
            int i = (int)(OutsideRectangle.Width / 40)-1;

            while (i >= 0)
            {
                System.Windows.Shapes.Rectangle rect1 = new System.Windows.Shapes.Rectangle();
                rect1.Height = 40;
                rect1.Width = 40;
                rect1.Fill = new ImageBrush {
                    ImageSource = new BitmapImage(new Uri(@"..\..\..\ClassLibrary2\Resources\CentreRock.png", UriKind.Relative))
                };
                Canvas.SetLeft(rect1, OutsideRectangle.Position_X - OutsideRectangle.Width / 2 +i*40);
                Canvas.SetTop(rect1, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2);

                World.Children.Add(rect1);
                i = i - 1;
            }

        }
    }
}
