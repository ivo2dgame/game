﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shapes;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.IO;
using System.Drawing;
using Enums;

namespace Objects
{
    public class AliveObject
    {
        protected int direction = 0;
        protected int Last_Coll_direction = 0;
        public double Position_X ;
        public double Position_Y ;
        public double Width1  ;
        public double Height1  ;

        public double Width2;
        public double Height2 ;
        protected double totspeedx;
        protected double totspeedy;
        public double Acceleration_X;
        public double Acceleration_X1;
        public double Acceleration_Y;
        public double Acceleration_Y1;

        public rectangle OutsideRectangle;
        public rectangle InsideRectangle;

        private const int REFRESHINTERVAL = 5;
        protected double Deltatime = (REFRESHINTERVAL / 1000.0);


        public bool IsAlive = false;

        public AliveObject()
        {
        }
        public void BerekenNieuwePositie()
        {

            totspeedx = InsideRectangle.Totspeed * Math.Cos(InsideRectangle.Moving_Direction) + (Acceleration_X * Deltatime / 2);
            totspeedx = totspeedx - totspeedx * 0.05;
            totspeedy = InsideRectangle.Totspeed * Math.Sin(InsideRectangle.Moving_Direction)+(Acceleration_Y * Deltatime / 2);
            
            InsideRectangle.Moving_Direction = Math.Atan2(totspeedy, totspeedx);
            InsideRectangle.Totspeed = Math.Sqrt(Math.Pow(totspeedx, 2) + Math.Pow(totspeedy, 2));
       

            InsideRectangle.Speed_x = InsideRectangle.Totspeed * Math.Cos(InsideRectangle.Moving_Direction);
            InsideRectangle.Speed_y = InsideRectangle.Totspeed * Math.Sin(InsideRectangle.Moving_Direction);
            OutsideRectangle.Speed_x = InsideRectangle.Speed_x;
            OutsideRectangle.Speed_y = InsideRectangle.Speed_y;

            InsideRectangle.Position_X = totspeedx * Deltatime + InsideRectangle.Position_X;
            InsideRectangle.Position_Y = totspeedy * Deltatime + InsideRectangle.Position_Y;
            OutsideRectangle.Position_X = InsideRectangle.Position_X;
            OutsideRectangle.Position_Y = InsideRectangle.Position_Y;


            Acceleration_Y = Acceleration_Y1;

        }
        public void SpeedChangeCollision(int direction, double indentation)
        {
            this.direction = direction;
            if (direction != 0)
            {
                this.Last_Coll_direction = direction;
            }
            if ((direction & (int)CollisionDirection.boven) == (int)(CollisionDirection.boven))
            {
                if (totspeedy <= 1)
                {
                    totspeedy = 0;
                    InsideRectangle.Position_Y = InsideRectangle.Position_Y + indentation;
                }
            }
            if ((direction & (int)CollisionDirection.Links) == (int)(CollisionDirection.Links))
            {
                if (totspeedx <= 1)
                {
                    totspeedx = 0;
                    InsideRectangle.Position_X = InsideRectangle.Position_X + indentation;
                }
            }
            if ((direction & (int)CollisionDirection.Rechts) == (int)(CollisionDirection.Rechts))
            {
                if (totspeedx >= -1)
                {
                    totspeedx = 0;
                    InsideRectangle.Position_X = InsideRectangle.Position_X - indentation;
                }
            }
            if ((direction & (int)CollisionDirection.Onder) == (int)(CollisionDirection.Onder))
            {
                if (totspeedy >= -1)
                {
                    totspeedy = 0;
                    Acceleration_Y = 0;
                    InsideRectangle.Position_Y = InsideRectangle.Position_Y - indentation;
                }
            }

            InsideRectangle.Moving_Direction = Math.Atan2(totspeedy, totspeedx);
            InsideRectangle.Totspeed = Math.Sqrt(Math.Pow(totspeedx, 2) + Math.Pow(totspeedy, 2));


        }
        
    }
    public class Non_AliveObject
    {

        public double Position_X;
        public double Position_Y;
        public double Width;
        public double Height;

        public double Acceleration_X;
        public double Acceleration_Y;

        public rectangle OutsideRectangle;

        public Non_AliveObject()
        {
                
        }

        private const int REFRESHINTERVAL = 5;
        double Deltatime = (REFRESHINTERVAL / 1000.0);
                
        public void BerekenNieuwePositie()
        {
            double totspeedx = OutsideRectangle.Totspeed * Math.Cos(OutsideRectangle.Moving_Direction) + (OutsideRectangle.Acceleration_x * Deltatime / 2);
            double totspeedy = OutsideRectangle.Totspeed * Math.Sin(OutsideRectangle.Moving_Direction) + (OutsideRectangle.Acceleration_x * Deltatime / 2);

            OutsideRectangle.Moving_Direction = Math.Atan2(totspeedy, totspeedx);
            OutsideRectangle.Totspeed = Math.Sqrt(Math.Pow(totspeedx, 2) + Math.Pow(totspeedy, 2));


            OutsideRectangle.Position_X = totspeedx * Deltatime + OutsideRectangle.Position_X;
            OutsideRectangle.Position_Y = totspeedy * Deltatime + OutsideRectangle.Position_Y;

            OutsideRectangle.Speed_x = OutsideRectangle.Totspeed * Math.Cos(OutsideRectangle.Moving_Direction);
            OutsideRectangle.Speed_y = OutsideRectangle.Totspeed * Math.Sin(OutsideRectangle.Moving_Direction);

            //OutsideRectangle.Point1.Position_X = OutsideRectangle.Position_X - Width / 2;
            //OutsideRectangle.Point1.Position_Y = OutsideRectangle.Position_Y - Height / 2;
            //OutsideRectangle.Point2.Position_X = OutsideRectangle.Position_X + Width / 2;
            //OutsideRectangle.Point2.Position_Y = OutsideRectangle.Position_Y - Height / 2;
            //OutsideRectangle.Point3.Position_X = OutsideRectangle.Position_X + Width / 2;
            //OutsideRectangle.Point3.Position_Y = OutsideRectangle.Position_Y + Height / 2;
            //OutsideRectangle.Point4.Position_X = OutsideRectangle.Position_X - Width / 2;
            //OutsideRectangle.Point4.Position_Y = OutsideRectangle.Position_Y + Height / 2;


        }
          

      

    }
    
   

   

    





}
