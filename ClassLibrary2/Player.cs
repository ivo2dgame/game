﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objects;
using shapes;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using Enums;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.Media;

namespace Objects
{
    public class Player : AliveObject
    {
        //System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
        //System.Windows.Shapes.Rectangle rect2 = new System.Windows.Shapes.Rectangle();
        public Player()
        {
            Position_X = 500;
            Position_Y = 500;
            Width1 = 38;
            Height1 = 48;
            Width2 = 30;
            Height2 = 35;

            Acceleration_X1 = 0;
            Acceleration_Y1 = 5000;
            Acceleration_Y = Acceleration_Y1;

            OutsideRectangle = new rectangle(Position_X, Position_Y, Width1, Height1);
            InsideRectangle = new rectangle(Position_X, Position_Y, Width2, Height2);
        }
        public void drawplayer(Canvas World)
        {
            //// ik zal de rechthoeken tekenen om de bewegingen te kunnen testen
            //// drawing outside rectangle 

            //rect.Height = OutsideRectangle.Height;
            //rect.Width = OutsideRectangle.Width;

            //rect.Fill = new SolidColorBrush(Colors.Red);
            //Canvas.SetLeft(rect, OutsideRectangle.Position_X - OutsideRectangle.Width / 2);
            //Canvas.SetTop(rect, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2);
            //World.Children.Add(rect);

            //// drawing inside rectangle 

            //rect2.Height = InsideRectangle.Height;
            //rect2.Width = InsideRectangle.Width;

            //rect2.Fill = new SolidColorBrush(Colors.Blue);
            //Canvas.SetLeft(rect2, InsideRectangle.Position_X - InsideRectangle.Width / 2);
            //Canvas.SetTop(rect2, InsideRectangle.Position_Y - InsideRectangle.Height / 2);
            //World.Children.Add(rect2);

            System.Windows.Shapes.Rectangle rect1 = new System.Windows.Shapes.Rectangle();
                rect1.Height = 48;
            
                rect1.Width = 38;
                rect1.Fill = new ImageBrush
            {
                    ImageSource = new BitmapImage(new Uri(@"..\..\..\ClassLibrary2\Resources\player.png", UriKind.Relative))
                };
                Canvas.SetLeft(rect1, OutsideRectangle.Position_X - OutsideRectangle.Width / 2);
                Canvas.SetTop(rect1, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2);

                World.Children.Add(rect1);
              

        }
        public void SpeedChangeLeft()
        {
            if ((direction & (int)CollisionDirection.Links) != (int)(CollisionDirection.Links))
            {
                totspeedx = -600;
                InsideRectangle.Moving_Direction = Math.Atan2(totspeedy, totspeedx);
                InsideRectangle.Totspeed = Math.Sqrt(Math.Pow(totspeedx, 2) + Math.Pow(totspeedy, 2));
            }
        }
        public void SpeedChangeRight()
        {
            if ((direction & (int)CollisionDirection.Rechts) != (int)(CollisionDirection.Rechts))
            {
                totspeedx = +600;
                InsideRectangle.Moving_Direction = Math.Atan2(totspeedy, totspeedx);
                InsideRectangle.Totspeed = Math.Sqrt(Math.Pow(totspeedx, 2) + Math.Pow(totspeedy, 2));
            }
        }
        public void SpeedChangeJump()
        {
            if ((Last_Coll_direction & (int)CollisionDirection.Onder) == (int)(CollisionDirection.Onder))
            {
                if (totspeedy <= 1)
                {
                    totspeedy = -1200;
                    InsideRectangle.Moving_Direction = Math.Atan2(totspeedy, totspeedx);
                    InsideRectangle.Totspeed = Math.Sqrt(Math.Pow(totspeedx, 2) + Math.Pow(totspeedy, 2));
                    Last_Coll_direction = 0;
                }
            }
        }
        public void SpeedChangedown()
        {
            if (totspeedy <= -1)
            {
                totspeedy = 600;
                InsideRectangle.Moving_Direction = Math.Atan2(totspeedy, totspeedx);
                InsideRectangle.Totspeed = Math.Sqrt(Math.Pow(totspeedx, 2) + Math.Pow(totspeedy, 2));
                
            }
        }
    }
}
