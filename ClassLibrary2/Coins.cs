﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shapes;
using System.Windows.Controls;
using System.Windows.Media;
using System.Drawing;
using System.Windows.Media.Imaging;
using Enums;
using System.Media;

namespace Objects
{
    public class Coins : Non_AliveObject
    {
        SoundPlayer simpleSound = new SoundPlayer(@"..\..\..\ClassLibrary2\Resources\Coin2.wav");
        

        public Coins()
        {
            Width = 17;
            Height = 17;
        }

        public void newPosition(int Random, bool firstdraw)
        {

            if(Random == 1)
            {
                Position_X = 600;
                Position_Y = 440;
            }
            else if(Random == 2)
            {
                Position_X = 200;
                Position_Y = 440;
            }
            else if (Random == 3)
            {
                Position_X = 700;
                Position_Y = 240;
            }
            else if (Random == 4)
            {
                Position_X = 100;
                Position_Y = 240;
            }
            else if (Random == 5)
            {
                Position_X = 400;
                Position_Y = 90;
            }
            else
            {
                Position_X = 400;
                Position_Y = 300;
            }

            if (firstdraw == false)
            {
                simpleSound.Play();
            }

            OutsideRectangle = new rectangle(Position_X, Position_Y, Width, Height);
        }

        public void draw(Canvas World)
        {

            System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
            System.Windows.Shapes.Rectangle rect2 = new System.Windows.Shapes.Rectangle();

            rect.Height = 30;
            //OutsideRectangle.Height;
            rect.Width = 30;
            //OutsideRectangle.Width;

            rect.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"..\..\..\ClassLibrary2\Resources\gem.png", UriKind.Relative))
            };
            //rect.Fill = new SolidColorBrush(Colors.Yellow);

            Canvas.SetLeft(rect, OutsideRectangle.Position_X - OutsideRectangle.Width / 2 - 7);
            Canvas.SetTop(rect, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2);
            World.Children.Add(rect);

            //image of rock
            //int i = (int)(OutsideRectangle.Width / 40) - 1;

            //while (i >= 0)
            //{
            //    System.Windows.Shapes.Rectangle rect1 = new System.Windows.Shapes.Rectangle();
            //    rect1.Height = 40;
            //    rect1.Width = 40;
            //    rect1.Fill = new ImageBrush
            //    {
            //        ImageSource = new BitmapImage(new Uri(@"..\..\..\ClassLibrary2\Resources\CentreRock.png", UriKind.Relative))
            //    };
            //    Canvas.SetLeft(rect1, OutsideRectangle.Position_X - OutsideRectangle.Width / 2 + i * 40);
            //    Canvas.SetTop(rect1, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2);

            //    World.Children.Add(rect1);
            //    i = i - 1;
            //}

        }
    }

}
