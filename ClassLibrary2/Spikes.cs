﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shapes;
using System.Windows.Controls;
using System.Windows.Media;
using System.Drawing;
using System.Windows.Media.Imaging;
using Enums;
using System.Threading;
using System.Diagnostics;
using System.Media;

namespace Objects
{

    public class Spikes : Non_AliveObject
    {

        Timer timer;
        List<Spikes> spikeList;
        Random random = new Random();

        //public Spikes(bool NewSpikeCreatorActive, List<Spikes> Spikes)
        //{
        //    if (NewSpikeCreatorActive == true)
        //    {
        //        Width = 40;
        //        Height = 40;
        //        timer = new Timer(new TimerCallback(SpawnNewSpike), null, 0, Spawninterval);
        //        spikeList = Spikes;
        //        OutsideRectangle = new rectangle(Position_X, Position_Y, Width, Height);
        //        SetPosition();
        //    }
        //}


        public Spikes(List<Spikes> Spikes)
        {
            Height = 40;
            Width = 40;
            spikeList = Spikes;
            OutsideRectangle = new rectangle(Position_X, Position_Y, Width, Height);
            SetPosition();
        }

        public Spikes()
        {
            Height = 30;
            Width = 20;
            OutsideRectangle = new rectangle(Position_X, Position_Y, Width, Height);
            SetPosition();
        }

        public Spikes (int xpos, int ypos)
        {
            Height = 30;
            Width = 20;
            OutsideRectangle = new rectangle(xpos, ypos, Width, Height);
        }

        public void SpawnNewSpike(object o)
        {
            Spikes spike = new Spikes(spikeList);
        }

        private void SetPosition()
        {
            int position = (int)(random.NextDouble()*24);
            
           switch (position)
            {
                case 0:
                    OutsideRectangle.Position_X = 120;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 1:
                    OutsideRectangle.Position_X = 160;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 2:
                    OutsideRectangle.Position_X = 200;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 3:
                    OutsideRectangle.Position_X = 240;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 4:
                    OutsideRectangle.Position_X = 280;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 5:
                    OutsideRectangle.Position_X = 520;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 6:
                    OutsideRectangle.Position_X = 560;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 7:
                    OutsideRectangle.Position_X = 600;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 8:
                    OutsideRectangle.Position_X = 640;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 9:
                    OutsideRectangle.Position_X = 680;
                    OutsideRectangle.Position_Y = 500;
                    break;
                case 10:
                    OutsideRectangle.Position_X = 20;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 11:
                    OutsideRectangle.Position_X = 60;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 12:
                    OutsideRectangle.Position_X = 100;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 13:
                    OutsideRectangle.Position_X = 140;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 14:
                    OutsideRectangle.Position_X = 180;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 15:
                    OutsideRectangle.Position_X = 620;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 16:
                    OutsideRectangle.Position_X = 660;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 17:
                    OutsideRectangle.Position_X = 700;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 18:
                    OutsideRectangle.Position_X = 740;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 19:
                    OutsideRectangle.Position_X = 780;
                    OutsideRectangle.Position_Y = 290;
                    break;
                case 20:
                    OutsideRectangle.Position_X = 320;
                    OutsideRectangle.Position_Y = 135;
                    break;
                case 21:
                    OutsideRectangle.Position_X = 360;
                    OutsideRectangle.Position_Y = 135;
                    break;
                case 22:
                    OutsideRectangle.Position_X = 400;
                    OutsideRectangle.Position_Y = 135;
                    break;
                case 23:
                    OutsideRectangle.Position_X = 440;
                    OutsideRectangle.Position_Y = 135;
                    break;
                case 24:
                    OutsideRectangle.Position_X = 480;
                    OutsideRectangle.Position_Y = 135;
                    break;
            }

        }

        public void draw(Canvas World)
        {

            // ik zal de rechthoeken tekenen om de bewegingen te kunnen testen
            // drawing outside rectangle 
            //System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
            //rect.Height = OutsideRectangle.Height;
            //rect.Width = OutsideRectangle.Width;

            //rect.Fill = new SolidColorBrush(Colors.Red);
            //Canvas.SetLeft(rect, OutsideRectangle.Position_X - OutsideRectangle.Width / 2);
            //Canvas.SetTop(rect, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2);
            //World.Children.Add(rect);

            System.Windows.Shapes.Rectangle rect2 = new System.Windows.Shapes.Rectangle();

            rect2.Height = 40;
            rect2.Width = 40;

            rect2.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"..\..\..\ClassLibrary2\Resources\Game_Spike.png", UriKind.Relative))
            };
            
            Canvas.SetLeft(rect2, OutsideRectangle.Position_X - OutsideRectangle.Width / 2 -9);
            Canvas.SetTop(rect2, OutsideRectangle.Position_Y - OutsideRectangle.Height / 2 -4);
            World.Children.Add(rect2);
        }
        public void Dispose()
        {
           
            if (timer != null)
            {
                timer = null;
                timer.Dispose();
            }
            GC.Collect();
            Debug.WriteLine("timer destroyed");
        }
        
    }
}

