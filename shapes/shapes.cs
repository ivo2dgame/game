﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace shapes
{
    public class shapes
    {
        #region Variables 
        // positions
       protected double position_x;
       protected double position_y;
       protected double moving_Direction;
       protected double massa = 1;
       
        
        public double Position_X
        {
            get { return position_x; }
            set { position_x = value; }

        }
        public double Position_Y
        {
            get { return position_y; }
            set { position_y = value; }

        }
        public double Moving_Direction
        {
            get { return moving_Direction; }
            set { moving_Direction = value; }

        }
        public double Massa
        {
            get { return massa; }
            set { massa = value; }

        }


        // speed
        protected double speed_x;
        protected double speed_y;
        protected double totspeed;

        public double Speed_x
        {
            get { return speed_x; }
            set { speed_x = value; }
        }
        public double Speed_y
        {
            get { return speed_y; }
            set { speed_y = value; }
        }
        public double Totspeed
        {
            get { return totspeed; }
            set { totspeed = value; }
        }

        // acceleration
        protected double acceleration_x;
        protected double acceleration_y;

        public double Acceleration_x
        {
            get { return acceleration_x; }
            set { acceleration_x = value; }
        }
        public double Acceleration_y
        {
            get { return acceleration_y; }
            set { acceleration_y = value; }
        }

        protected double rotation;  // clockwise
        public double Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }
        #endregion
        public shapes()
        {

        }          
    }

    public class Circles : shapes
    {
        private const int REFRESHINTERVAL = 5;
        public Timer timer;
        int radius;        
        public int Radius
        {
            get { return radius; }
            set { radius = value; }
        }
        public Circles()
        {
            timer = new Timer(new TimerCallback(BerekenNieuwePositie), null, 0, REFRESHINTERVAL);
        }
        private void BerekenNieuwePositie(object o)
        {
            double Deltatime = (REFRESHINTERVAL / 1000.0);



            double totspeedx = totspeed * Math.Cos(moving_Direction) - (acceleration_x * Deltatime / 2);
            double totspeedy = totspeed * Math.Sin(moving_Direction) - (acceleration_y * Deltatime / 2);

            position_x = totspeed * Math.Cos(moving_Direction) * Deltatime + position_x;
            position_y = totspeed * Math.Sin(moving_Direction) * Deltatime + position_y;

            speed_x = totspeed * Math.Cos(moving_Direction);
            speed_y = totspeed * Math.Sin(moving_Direction);




        }

    }
    public class triangle : shapes
    {
        private const int REFRESHINTERVAL = 5;
        public Timer timer;
        private double point1D;
        private double point1Alfa;
        private double point2D;
        private double point2Alfa;
        private double point3D;
        private double point3Alfa;

        public double Point1x
        {
            get {return (point1D * Math.Cos(point1Alfa + Rotation));}
            
        }
        public double Point1y
        {
            get { return (point1D * Math.Sin(point1Alfa + Rotation)); }
            
        }
        public double Point2x
        {
            get { return (point2D * Math.Cos(point2Alfa + Rotation)); }

        }
        public double Point2y
        {
            get { return (point2D * Math.Sin(point2Alfa + Rotation)); }

        }
        public double Point3x
        {
            get { return (point3D * Math.Cos(point3Alfa + Rotation)); }

        }
        public double Point3y
        {
            get { return (point3D * Math.Sin(point3Alfa + Rotation)); }

        }

        public triangle(double point1x, double point1Alfa, double point2x, double point2Alfa, double point3x, double point3Alfa)
        {
            this.point1D = point1x;
            this.point1Alfa = point1Alfa;
            this.point2D = point2x;
            this.point2Alfa = point2Alfa;
            this.point3D = point3x;
            this.point3Alfa = point3Alfa;

            double midpositionx = (Point1x + Point2x + Point3x) / 3;
            double midpositiony = (Point1y + Point2y + Point3y) / 3;

            point1Alfa = angle(Point1x, Point1y, midpositionx, midpositiony);
            point1x = distance(Point1x, Point1y, midpositionx, midpositiony);

            point2Alfa = angle(Point2x, Point2y, midpositionx, midpositiony);
            point2x = distance(Point2x, Point2y, midpositionx, midpositiony);

            point3Alfa = angle(Point3x, Point3y, midpositionx, midpositiony);
            point3x = distance(Point3x, Point3y, midpositionx, midpositiony);

            this.point1D = point1x;
            this.point1Alfa = point1Alfa;
            this.point2D = point2x;
            this.point2Alfa = point2Alfa;
            this.point3D = point3x;
            this.point3Alfa = point3Alfa;

            timer = new Timer(new TimerCallback(BerekenNieuwePositie), null, 0, REFRESHINTERVAL);
        }
        private double distance(double Pointx, double Pointy, double midpositionx, double midpositiony)
        {
            double x = Math.Sqrt(Math.Pow(Pointx - midpositionx, 2) + Math.Pow(Pointy - midpositiony, 2));
            return x;
        }
        private double angle(double Pointx, double Pointy, double midpositionx, double midpositiony)
        {
            double x;
            double Distancex = Pointx - midpositionx;
            double Distancey = Pointy - midpositiony;

               x = Math.Atan2(Distancey , Distancex);
            
            

            return x;

        }

        private void BerekenNieuwePositie(object o)
        {
            double Deltatime = (REFRESHINTERVAL / 1000.0);



            double totspeedx = totspeed * Math.Cos(moving_Direction) - (acceleration_x * Deltatime / 2);
            double totspeedy = totspeed * Math.Sin(moving_Direction) - (acceleration_y * Deltatime / 2);

            position_x = totspeed * Math.Cos(moving_Direction) * Deltatime + position_x;
            position_y = totspeed * Math.Sin(moving_Direction) * Deltatime + position_y;

            speed_x = totspeed * Math.Cos(moving_Direction);
            speed_y = totspeed * Math.Sin(moving_Direction);




        }





    }
    public class quadrangle : shapes
    {
        #region Variables

        private const int REFRESHINTERVAL = 5;
        public Timer timer;

        private double point1D;
        private double point1Alfa;
        private double point2D;
        private double point2Alfa;
        private double point3D;
        private double point3Alfa;
        private double point4D;
        private double point4Alfa;

        public double Point1x
        {
            get { return (point1D * Math.Cos(point1Alfa + Rotation)); }

        }
        public double Point1y
        {
            get { return (point1D * Math.Sin(point1Alfa + Rotation)); }

        }
        public double Point2x
        {
            get { return (point2D * Math.Cos(point2Alfa + Rotation)); }

        }
        public double Point2y
        {
            get { return (point2D * Math.Sin(point2Alfa + Rotation)); }

        }
        public double Point3x
        {
            get { return (point3D * Math.Cos(point3Alfa + Rotation)); }

        }
        public double Point3y
        {
            get { return (point3D * Math.Sin(point3Alfa + Rotation)); }

        }
        public double Point4x
        {
            get { return (point4D * Math.Cos(point4Alfa + Rotation)); }

        }
        public double Point4y
        {
            get { return (point4D * Math.Sin(point4Alfa + Rotation)); }

        }
        #endregion

        public quadrangle(double point1x, double point1Alfa, double point2x, double point2Alfa, double point3x, double point3Alfa, double point4x, double point4Alfa)
        {
            this.point1D = point1x;
            this.point1Alfa = point1Alfa;
            this.point2D = point2x;
            this.point2Alfa = point2Alfa;
            this.point3D = point3x;
            this.point3Alfa = point3Alfa;
            this.point3D = point4x;
            this.point3Alfa = point4Alfa;

            double midpositionx = (Point1x + Point2x + Point3x+ Point4x) / 3;
            double midpositiony = (Point1y + Point2y + Point3y+ Point4y) / 3;

            point1Alfa = angle(Point1x, Point1y, midpositionx, midpositiony);
            point1x = distance(Point1x, Point1y, midpositionx, midpositiony);

            point2Alfa = angle(Point2x, Point2y, midpositionx, midpositiony);
            point2x = distance(Point2x, Point2y, midpositionx, midpositiony);

            point3Alfa = angle(Point3x, Point3y, midpositionx, midpositiony);
            point3x = distance(Point3x, Point3y, midpositionx, midpositiony);

            point4Alfa = angle(Point4x, Point4y, midpositionx, midpositiony);
            point4x = distance(Point4x, Point4y, midpositionx, midpositiony);

            this.point1D = point1x;
            this.point1Alfa = point1Alfa;
            this.point2D = point2x;
            this.point2Alfa = point2Alfa;
            this.point3D = point3x;
            this.point3Alfa = point3Alfa;
            this.point4D = point4x;
            this.point4Alfa = point4Alfa;
            
            timer = new Timer(new TimerCallback(BerekenNieuwePositie), null, 0, REFRESHINTERVAL);
        }
        public quadrangle(double point1x, double point1Alfa, double point2x, double point2Alfa, double point3x, double point3Alfa, double point4x, double point4Alfa,double accellerationx,double accellerationy)
        {
            this.point1D = point1x;
            this.point1Alfa = point1Alfa;
            this.point2D = point2x;
            this.point2Alfa = point2Alfa;
            this.point3D = point3x;
            this.point3Alfa = point3Alfa;
            this.point3D = point4x;
            this.point3Alfa = point4Alfa;

            double midpositionx = (Point1x + Point2x + Point3x + Point4x) / 3;
            double midpositiony = (Point1y + Point2y + Point3y + Point4y) / 3;

            point1Alfa = angle(Point1x, Point1y, midpositionx, midpositiony);
            point1x = distance(Point1x, Point1y, midpositionx, midpositiony);

            point2Alfa = angle(Point2x, Point2y, midpositionx, midpositiony);
            point2x = distance(Point2x, Point2y, midpositionx, midpositiony);

            point3Alfa = angle(Point3x, Point3y, midpositionx, midpositiony);
            point3x = distance(Point3x, Point3y, midpositionx, midpositiony);

            point4Alfa = angle(Point4x, Point4y, midpositionx, midpositiony);
            point4x = distance(Point4x, Point4y, midpositionx, midpositiony);

            this.point1D = point1x;
            this.point1Alfa = point1Alfa;
            this.point2D = point2x;
            this.point2Alfa = point2Alfa;
            this.point3D = point3x;
            this.point3Alfa = point3Alfa;
            this.point4D = point4x;
            this.point4Alfa = point4Alfa;

            this.acceleration_x = accellerationx;
            this.acceleration_y = accellerationy;

            timer = new Timer(new TimerCallback(BerekenNieuwePositie), null, 0, REFRESHINTERVAL);
        }
        private double distance(double Pointx, double Pointy, double midpositionx, double midpositiony)
        {
            double x = Math.Sqrt(Math.Pow(Pointx - midpositionx, 2) + Math.Pow(Pointy - midpositiony, 2));
            return x;
        }
        private double angle(double Pointx, double Pointy, double midpositionx, double midpositiony)
        {
            double x;
            double Distancex = Pointx - midpositionx;
            double Distancey = Pointy - midpositiony;

            x = Math.Atan2(Distancey, Distancex);



            return x;

        }
        private void BerekenNieuwePositie(object o)
        {
            double Deltatime = (REFRESHINTERVAL / 1000.0);

          

            double totspeedx = totspeed * Math.Cos(moving_Direction) - (acceleration_x * Deltatime / 2);
            double totspeedy = totspeed * Math.Sin(moving_Direction) - (acceleration_y * Deltatime / 2);

            position_x = totspeed * Math.Cos(moving_Direction) * Deltatime + position_x;
            position_y = totspeed * Math.Sin(moving_Direction) * Deltatime + position_y;

            speed_x = totspeed * Math.Cos(moving_Direction);
            speed_y = totspeed * Math.Sin(moving_Direction);

       


        }
    }
    public class rectangle : shapes
    {
        private const int REFRESHINTERVAL = 5;
      
        private double height;
        private double width;

        public double  Height
        {
            get
            {
                return height;
            }
        }
        public double Width
        {
            get
            {
                return width;
            }
        }

        List<point> Points = new List<point>();

        public point Point1= new point();
        public point Point2 = new point();
        public point Point3 = new point();
        public point Point4 = new point();

        public rectangle()
        {}

        public rectangle(double position_x, double position_y, double width, double height)
        {
            this.position_x = position_x;
            this.position_y = position_y;
            this.height = height;
            this.width  = width;

            Point1.Position_X = position_x - width / 2;
            Point1.Position_Y = position_y - height / 2;
            Point2.Position_X = position_x + width / 2;
            Point2.Position_Y = position_y - height / 2;
            Point3.Position_X = position_x + width / 2;
            Point3.Position_Y = position_y + height / 2;
            Point4.Position_X = position_x - width / 2;
            Point4.Position_Y = position_y + height / 2;
        }
       

   }
    public class point
    {
        private double position_x;
        private double position_y;

        public double Position_X
            {
            set { position_x = value; }
            get { return position_x; }
            }

        public double Position_Y
        {
            set { position_y = value; }
            get { return position_y; }
        }
        public point()
        {

        }
        public point(double Position_x, double Position_y)
        {
            position_x = Position_x;
            position_y = Position_y;
        }



    }
    }

