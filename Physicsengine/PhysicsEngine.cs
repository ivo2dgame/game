﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using shapes;
using Enums;
using System.Diagnostics;

namespace Physicsengine
{

    public class Collisiondetection
    {

        /// <summary>
        ///  /// CollisionDetection with 2circles
        /// </summary>
        /// <param name="circle1"></param>
        /// <param name="circle2"></param>
        public static bool CollisionDetection(Circles circle1, Circles circle2)
        {
            bool collision = false;
            double DeltaX = (circle1.Position_X - circle2.Position_X) * (circle1.Position_X - circle2.Position_X);
            double DeltaY = (circle1.Position_Y - circle2.Position_Y) * (circle1.Position_Y - circle2.Position_Y);
            double MaxDistance = Math.Pow((circle1.Radius + circle2.Radius),2) ;
           

            if (DeltaX + DeltaY <= MaxDistance)
            {
                

                bool IsCollisionPos = Physicsengine.IsCollisionPos(circle1, circle2);
               
                if (IsCollisionPos)
                {
                   
                    collision = true;

                }
                
            }
            return collision;
        }
        /// <summary>
        /// CollisionDetection with the edge of the world
        /// </summary>
        /// <param name="circle1"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>    
        public static void CollisionDetection(Circles circle, triangle triangle)
        {
            double PositionX = circle.Position_X;
            double PositionY = circle.Position_Y;
            double Radius = circle.Radius;


            double point1x = triangle.Point1x;
            double point1y = triangle.Point1y;
            double point2x = triangle.Point2x;
            double point2y = triangle.Point2y;
            double point3x = triangle.Point3x;
            double point3y = triangle.Point3y;

            // detection if a vertex is in the circle

            if (CollisionDetection(circle, point1x, point1y) || CollisionDetection(circle, point1x, point1y) || CollisionDetection(circle, point1x, point1y))
                {

                 
                
               

            }
            // detection if circle on the edge of the triangle



        }
        public static bool CollisionDetection(Circles circle, double Pointx, double Pointy)
        {
            bool Collision = false;
            double Max_Range = Math.Pow(circle.Radius, 2);
            double distance = Math.Pow((circle.Position_X - Pointx), 2) + Math.Pow((circle.Position_Y - Pointy), 2);
            if (distance< Max_Range)
            {
                Collision = true;
            }
            return Collision;      
        }
        public static int CollisionDetection(rectangle Rect1, rectangle Rect2)
        {
           

            int richting=0;

            if (    Rect1.Position_X + Rect1.Width / 2 > Rect2.Position_X - Rect2.Width / 2
                 && Rect1.Position_X - Rect1.Width / 2 < Rect2.Position_X + Rect2.Width / 2
                 && Rect1.Position_Y + Rect1.Height / 2 > Rect2.Position_Y - Rect2.Height / 2
                 && Rect1.Position_Y - Rect1.Height / 2 < Rect2.Position_Y + Rect2.Height / 2)
            {
                if ((Rect1.Position_Y - Rect1.Height / 2) - (Rect2.Position_Y + Rect2.Height / 2) < 0 & (Rect1.Position_Y - Rect1.Height / 2) - (Rect2.Position_Y + Rect2.Height / 2) > -4)
                {
                    //collision Boven
                    richting = richting + (int)CollisionDirection.boven;
                    Debug.WriteLine("collision boven");
                }
                if ((Rect1.Position_Y + Rect1.Height / 2) - (Rect2.Position_Y - Rect2.Height / 2) > 0 & (Rect1.Position_Y + Rect1.Height / 2) - (Rect2.Position_Y - Rect2.Height / 2) < 4)
                {
                    //collision Onder
                    richting = richting + (int)CollisionDirection.Onder;
                    Debug.WriteLine("collision onder");
                }
                if ((Rect1.Position_X - Rect1.Width / 2) - (Rect2.Position_X + Rect2.Width/2)   < 0 & (Rect1.Position_X - Rect1.Width / 2) - (Rect2.Position_X + Rect2.Width / 2) > -4)
                {
                    //collision Links
                    richting = richting + (int)CollisionDirection.Links;
                    Debug.WriteLine("collision Links");
                }
                if ((Rect1.Position_X + Rect1.Width / 2) - (Rect2.Position_X - Rect2.Width / 2) > 0 & (Rect1.Position_X + Rect1.Width / 2) - (Rect2.Position_X - Rect2.Width / 2) < 4)
                {
                    //collision Rechts
                    richting = richting + (int)CollisionDirection.Rechts;
                    Debug.WriteLine("collision Rechts");
                }
 
                if(richting==0 )
                {
                    if (Rect1.Speed_y > 0)
                    {
                        richting = richting + (int)CollisionDirection.Onder;
                        Debug.WriteLine("collision onder door geen andere oplossingen");
                    }
                    if (Rect1.Speed_y < 0)
                    {
                        richting = richting + (int)CollisionDirection.boven;
                        Debug.WriteLine("collision boven door geen andere oplossingen");
                    }
                }
            }
          return richting;
        }
        public static int CollisionDetection(rectangle Rect1,  double maxposx, double maxposy)
        {
            int richting = 0;
            if (Rect1.Position_Y >= maxposy - Rect1.Height / 2)
            {
                if (Rect1.Speed_y>=0)
                {
                 richting = richting+(int)CollisionDirection.Onder;
                }
            }
            if (Rect1.Position_Y <=  Rect1.Height/2)
            {
                if (Rect1.Speed_y <= 0)
                {
                    richting = richting + (int)CollisionDirection.boven;
                }
                
            }
            if (Rect1.Position_X <= Rect1.Width / 2)
            {
                if (Rect1.Speed_x <= 1)
                {
                    richting = richting + (int)CollisionDirection.Links;
                }
               
            }
            if (Rect1.Position_X >= maxposx - Rect1.Width / 2)
            {
                if (Rect1.Speed_x >= 0)
                {
                    richting = richting + (int)CollisionDirection.Rechts;
                }
               
            }



            return richting;
        }
        public static double CollisionIndentation(int direction, rectangle Rect1, rectangle Rect2)
        {
            double indentation = 0;
            int teller = 0;
            if ((direction & (int)CollisionDirection.boven) == (int)(CollisionDirection.boven))
            {
                indentation = -(Rect1.Position_Y - Rect1.Height / 2) + (Rect2.Position_Y + Rect2.Height / 2);
                teller = teller + 1;
            }
            if ((direction & (int)CollisionDirection.Onder) == (int)(CollisionDirection.Onder))
            {
                indentation = (Rect1.Position_Y + Rect1.Height / 2) - (Rect2.Position_Y - Rect2.Height / 2);
                teller = teller + 1;
            }
            if ((direction & (int)CollisionDirection.Links) == (int)(CollisionDirection.Links))
            {
                indentation = -(Rect1.Position_X - Rect1.Width / 2) + (Rect2.Position_X + Rect2.Width / 2);
                teller = teller + 1;
            }
            if ((direction & (int)CollisionDirection.Rechts) == (int)(CollisionDirection.Rechts))
            {
                indentation = (Rect1.Position_X + Rect1.Width / 2) - (Rect2.Position_X - Rect2.Width / 2);
                teller = teller + 1;
            }

            if( teller >1 )
            {
                indentation = 0;
            }
            return indentation;
        }

        class Physicsengine
        {
            public static bool IsCollisionPos(Circles circle1, Circles circle2)
            {
                bool CollisionPos = false;
                //calculating if a collision is possible
                double positionx = circle2.Position_X- circle1.Position_X;
                double positiony = circle2.Position_Y- circle1.Position_Y;

                double vel1x = circle1.Totspeed * Math.Cos(circle1.Moving_Direction) - circle2.Totspeed * Math.Cos(circle2.Moving_Direction);
                double vel1y = circle1.Totspeed * Math.Sin(circle1.Moving_Direction) - circle2.Totspeed * Math.Sin(circle2.Moving_Direction);

                Math.Atan2(positionx, positiony);
                Math.Atan2(vel1x, vel1y);
                CollisionPos = Math.Atan2(positionx, positiony) < Math.Atan2(vel1x, vel1y) + 45 * Math.PI / 180 && Math.Atan2(positionx, positiony) > Math.Atan2(vel1x, vel1y) - 45 * Math.PI / 180;

                return CollisionPos;
            }

            private static double Circle1_ResultingSpeed(Circles circle1, Circles circle2, double v1, double v2)
            {
                double f = (v1 * (circle1.Massa - circle2.Massa) + 2 * circle2.Massa * v2) / (circle1.Massa + circle2.Massa);
                return f;
            }
            private static double Circle2_ResultingSpeed(Circles circle1, Circles circle2, double graden)
            {
                double speedx = circle1.Speed_x - circle2.Speed_x;
                double speedy = circle1.Speed_y - circle2.Speed_y;

                double OnNormal = Math.Sin(graden) * speedx + Math.Sin(graden + 90) * speedy;
                return OnNormal;
            }
            public static void ChangeSpeed(Circles circle1, Circles circle2)
            {
                 // calculating the velocity change
            double ImpulsHoek = Math.Atan2(circle1.Position_Y - circle2.Position_Y, circle1.Position_X - circle2.Position_X);

            double v1x = circle1.Totspeed * Math.Cos(circle1.Moving_Direction - ImpulsHoek);
            double v1y = circle1.Totspeed * Math.Sin(circle1.Moving_Direction - ImpulsHoek);
            double v2x = circle2.Totspeed * Math.Cos(circle2.Moving_Direction - ImpulsHoek);
            double v2y = circle2.Totspeed * Math.Sin(circle2.Moving_Direction - ImpulsHoek);

            double f1x = Circle1_ResultingSpeed(circle1, circle2, v1x, v2x);
            double f2x = Circle1_ResultingSpeed(circle1, circle2, v2x, v1x);

            double f1y = Circle1_ResultingSpeed(circle1, circle2, v1y, v2y);
            double f2y = Circle1_ResultingSpeed(circle1, circle2, v2y, v1y);


            circle1.Totspeed = Math.Sqrt((f1x* f1x) + (f1y* f1y));
            circle2.Totspeed = Math.Sqrt((f2x* f2x) + (f2y* f2y));

            circle1.Moving_Direction = Math.Atan2(f1y, f1x) + ImpulsHoek;
            circle2.Moving_Direction = Math.Atan2(f2y, f2x) + ImpulsHoek;
            }
            public static void ChangeSpeed(Circles circle, triangle triangle)
            {
                
            }
            public static void ChangeSpeed(Circles circle, quadrangle square)
            {

            }
            public static void ChangeSpeed(rectangle rect1, rectangle rect2)
            {

            }

        }
    }
}
